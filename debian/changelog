libtest-perl-critic-perl (1.04-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 10:52:24 +0000

libtest-perl-critic-perl (1.04-3) unstable; urgency=medium

  * Backup/restore MANIFEST before/after tests, as it gets modified.
    (Closes: #1049032)
  * debian/watch: use uscan macros.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Wrap long lines in changelog entries: 1.04-2.

 -- gregor herrmann <gregoa@debian.org>  Fri, 08 Mar 2024 20:24:48 +0100

libtest-perl-critic-perl (1.04-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Mon, 10 Jul 2023 13:59:14 +0000

libtest-perl-critic-perl (1.04-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Remove constraints unnecessary since buster
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 15:41:01 +0000

libtest-perl-critic-perl (1.04-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:44:42 +0000

libtest-perl-critic-perl (1.04-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Update debian/upstream/metadata.
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 1.04.
  * Update upstream contact.
  * Update years of upstream and packaging copyright.
  * Update versioned (build) dependencies.
  * Declare compliance with Debian Policy 4.1.3.
  * Drop unneeded version constraints.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Mar 2018 23:28:46 +0200

libtest-perl-critic-perl (1.03-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata
  * Import upstream version 1.03
  * Update build dependencies
  * Update upstream copyright
  * Improve upstream metadata

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 15 Aug 2015 06:15:16 -0300

libtest-perl-critic-perl (1.02-2) unstable; urgency=low

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format "3.0 (quilt)".
  * Add explicit build dependency on libmodule-build-perl.
  * Bump debhelper compatibility level to 9.
  * Update years of packaging copyright.
  * Refresh license stanzas in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Jun 2015 18:56:11 +0200

libtest-perl-critic-perl (1.02-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Added myself to Uploaders
  * Standards-Version 3.8.3 (no changes)
  * Rewrote control description
  * Bump perlcritic version dep per upstream
  * Use new short debhelper rules format
  * Update copyright info from changelog trailers

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * debian/copyright: update upstream copyright notice.

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 23 Oct 2009 04:40:30 -0400

libtest-perl-critic-perl (1.01-2) unstable; urgency=low

  [ David Paleino ]
  * Removed myself from Uploaders (Closes: #509505)
  * debian/control:
    - Standards-Version 3.8.0 (no changes needed)
    - libperl-critic-perl dependency moved to Build-Depends-Indep
  * debian/copyright: using "Copyright" word for Debian packaging

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * debian/copyright: switch to new format.
  * Add perl-modules (>= 5.10) as an alternative build dependency to
    libmodule-build-perl.
  * Add libtest-pod-perl, libtest-pod-coverage-perl to Build-Depends-Indep to
    activate additional tests.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Jan 2009 16:53:20 +0100

libtest-perl-critic-perl (1.01-1) unstable; urgency=low

  * Initial Release.

 -- David Paleino <d.paleino@gmail.com>  Sun, 16 Dec 2007 02:23:24 +0100
